/**
 * @provides snipe-fields-validation-js
 */

function shipping_validations() {
  let styles = `
    .snipe_assets_select {
      display: block;
    }
    .item_card {
      border-bottom: 1px dashed #ccc;
      padding-bottom: 1em;
      margin-bottom: 1em;
      clear:both;
    }
  `;

  var styleSheet = document.createElement("style");
  styleSheet.type = "text/css";
  styleSheet.innerText = styles;
  document.head.appendChild(styleSheet);

  asset_id = document.getElementsByName("std:maniphest:shipping:asset-id")[0];
  asset_id.parentNode.parentNode.style.display = "none";

  items_container = document.createElement("div");
  items_container.setAttribute("id", "items_container");
  tags_container = document.createElement("div");
  items_container.appendChild(tags_container);
  assets = document.getElementById("snipe_assets");

  let asset_ids_array = asset_id.value.split(",");
  for (var i = 0; i < assets.options.length; i++) {
    assets.options[i].selected =
      asset_ids_array.indexOf(assets.options[i].value) >= 0;
  }

  let ship_to_customer = document.getElementsByName(
    "std:maniphest:shipping:ship-to-customer"
  )[0];
  ship_to_customer.onchange = function() {
    if (ship_to_customer.checked) {
      ship_to_customer.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.style.display =
        "none";
    } else {
      ship_to_customer.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.style.display =
        "block";
    }
  };

  let import_asset = document.getElementsByName(
    "std:maniphest:shipping:import-asset"
  )[0];
  let el_assets_value = document.getElementsByName(
    "std:maniphest:shipping:item-value"
  )[0];
  let el_country_of_origin = document.getElementsByName(
    "std:maniphest:shipping:item-origin"
  )[0];
  let el_currency = document.getElementsByName(
    "std:maniphest:shipping:currency"
  )[0];
  let el_description = document.getElementsByName(
    "std:maniphest:shipping:item-description"
  )[0];

  if (!import_asset.checked) {
    assets.parentNode.parentNode.style.display = "none";
  }

  import_asset.onchange = function() {
    if (import_asset.checked) {
      items_container.style.display = "block";
      assets.parentNode.parentNode.style.display = "block";
    } else {
      items_container.style.display = "none";
      assets.parentNode.parentNode.style.display = "none";
      items_container.innerText = "";
      assets.value = "";
      asset_id.value = "";
    }
  };

  assets.onchange = function() {
    document
      .querySelectorAll(".asset_tag, .item_card")
      .forEach(e => e.remove());
    el_description.value = "";

    let values = Array.from(assets.selectedOptions).map(v => v.value);
    let items = Array.from(assets.selectedOptions).map(option => {
      el = document.createElement("a");
      el.textContent = option.text;
      el.classList.add(
        "asset_tag",
        "jx-tokenizer-token",
        "jx-tokenizer-token-object",
        "green"
      );
      el.setAttribute("target", "blank");
      el.setAttribute("href", option.attributes["data-src"].value);

      close_btn = document.createElement("a");
      close_btn.className = "jx-tokenizer-x";
      close_btn.setAttribute("data-sigil", "remove");
      close_btn.text = "x";
      close_btn.onclick = function() {
        close_btn.parentNode.remove();
      };

      el.appendChild(close_btn);

      return {
        name: option.textContent,
        href: option.attributes["data-src"].value,
        props: {
          purchase_cost: {
            value: option.attributes["data-purchase-cost"].value,
            label: "Purchase cost"
          },
          currency: {
            value: option.attributes["data-currency"].value,
            label: "Currency"
          },
          country_of_origin: {
            value: option.attributes["data-country-of-origin"].value,
            label: "Country of origin"
          }
        }
      };
    });

    items.forEach(function(item) {
      item_element = document.createElement("div");
      item_element.className = "item_card";
      item_url = document.createElement("a");
      item_url.textContent = item["name"];
      item_url.setAttribute("target", "blank");
      item_url.setAttribute("href", item["href"]);
      item_element.appendChild(item_url);
      el_description.value = el_description.value + item["name"];
      for (var key in item.props) {
        prop = document.createElement("p");
        prop.textContent = item.props[key].label + ": " + item.props[key].value;
        item_element.appendChild(prop);

        el_description.value = el_description.value + "\n" + prop.textContent;
      }

      items_container.appendChild(item_element);
      el_description.value = el_description.value + "\n\n";

      if (items.length == 1) {
        el_country_of_origin.value = item.props.country_of_origin.value;
        el_currency.value = item.props.currency.value;
      }
    });

    if (items.length > 1) {
      el_country_of_origin.value = "";
      el_currency.value = "";
    }

    assets.parentNode.appendChild(items_container);

    el_assets_value.value = items.reduce(function(sum, i) {
      return sum + Number(i.props.purchase_cost.value);
    }, 0);
    asset_id.value = values;
  };
}

function fetch_and_hide_extra_assets_elements(index, unique_items) {
  asset = {};

  // This block of elements are used only by Snipe-IT
  asset["asset_name_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:asset-name" + index
  )[0].parentNode.parentNode;
  asset["asset_name_parent"].classList.add("snipe_property");

  asset["item_owner_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:item-owner" + index
  )[0].parentNode.parentNode;
  asset["item_owner_parent"].classList.add("snipe_property");

  asset["model_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:model" + index
  )[0].parentNode.parentNode;
  asset["model_parent"].classList.add("snipe_property");

  // Elements used by Phab and Snipe-IT
  asset["separator"] = asset["asset_name_parent"].previousSibling;
  asset["with"] = asset["item_owner_parent"].nextSibling;
  asset["quantity"] = document.getElementsByName(
    "std:maniphest:purchasing:quantity" + index
  )[0].parentNode.parentNode;
  asset["country_of_origin_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:country-origin" + index
  )[0].parentNode.parentNode;
  asset["asset_cost_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:expected-cost" + index
  )[0].parentNode.parentNode;

  asset["asset_link_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:item-link" + index
  )[0].parentNode.parentNode;

  asset["asset_currency_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:currency" + index
  )[0].parentNode.parentNode;

  Object.keys(asset).forEach(i => asset[i].classList.add("asset_property"));

  // Hide all extra items at first
  if (index > 1 && index > unique_items) {
    Object.keys(asset).forEach(i => (asset[i].style.display = "none"));
  }

  return asset;
}

function purchasing_validations() {
  let styles = `
      .add-item {
        float:left !important;
      }
  `;

  var styleSheet = document.createElement("style");
  styleSheet.type = "text/css";
  styleSheet.innerText = styles;
  document.head.appendChild(styleSheet);

  num_asset_forms = document.getElementsByName(
    "std:maniphest:purchasing:unique-items"
  )[0];
  if (num_asset_forms.length) {
    // If this field was not rendered, we can't make the validations and control
    // the form based on assets amount.
    return;
  }
  num_asset_forms.parentNode.parentNode.style.display = "none";

  assets_list = [];
  MAX_ALLOWED_ASSETS = 5;

  for (let i = 1; i <= MAX_ALLOWED_ASSETS; i++) {
    assets_list.push(
      fetch_and_hide_extra_assets_elements(i, num_asset_forms.value)
    );
  }

  let should_create = document.getElementsByName(
    "std:maniphest:purchasing:create-item"
  )[0];

  assets_properties = document.getElementsByClassName("asset_property");
  snipe_it_properties = document.getElementsByClassName("snipe_property");

  if (!should_create.checked) {
    for (let i = 0; i < snipe_it_properties.length; i++) {
      snipe_it_properties[i].style.display = "none";
    }
  }

  should_create.onchange = function() {
    if (should_create.checked) {
      for (let i = 0; i < num_asset_forms.value; i++) {
        for (let key in assets_list[i]) {
          if (assets_list[i][key].classList.contains("snipe_property")) {
            assets_list[i][key].style.display = "block";
          }
        }
      }
    } else {
      for (let i = 0; i < num_asset_forms.value; i++) {
        for (let key in assets_list[i]) {
          if (assets_list[i][key].classList.contains("snipe_property")) {
            assets_list[i][key].style.display = "none";
          }
        }
      }
    }
  };

  submit_parent = document.getElementsByName("__submit__")[0].parentNode
    .parentNode;
  add_new_asset = document.createElement("div");
  add_new_asset_btn = document.createElement("a");
  add_new_asset_btn.classList.add("button", "add-item");
  add_new_asset_btn.innerHTML = "Add another item to this ticket";
  add_new_asset.appendChild(add_new_asset_btn);

  add_new_asset_btn.onclick = function() {
    if (num_asset_forms.value < MAX_ALLOWED_ASSETS) {
      for (let key in assets_list[num_asset_forms.value - 1]) {
        if (
          assets_list[num_asset_forms.value][key].classList.contains(
            "snipe_property"
          )
        ) {
          if (should_create.checked) {
            assets_list[num_asset_forms.value][key].style.display = "block";
          }
        } else {
          assets_list[num_asset_forms.value][key].style.display = "block";
        }
      }

      num_asset_forms.value++;

      // Auto fill currency and ship to fields based on the first item
      document.getElementsByName(
        "std:maniphest:purchasing:currency" + num_asset_forms.value
      )[0].value = document.getElementsByName(
        "std:maniphest:purchasing:currency1"
      )[0].value;
    } else {
      add_new_asset_btn.innerText = "Items per ticket exceeded";
      add_new_asset_btn.classList.add("disabled");
    }
  };

  submit_parent.appendChild(add_new_asset);
}

document.addEventListener("DOMContentLoaded", function(event) {
  /* Make sure this will load only in the Purchase and Shipping forms */
  let purchasing_element = document.getElementsByName(
    "std:maniphest:purchasing:asset-name1"
  );
  let shipping_element = document.getElementsByName(
    "std:maniphest:shipping:shipper-name"
  );

  if (purchasing_element.length) {
    try {
      purchasing_validations();
    } catch (error) {
      console.error(error);
    }
  } else if (shipping_element.length) {
    try {
      shipping_validations();
    } catch (error) {
      console.error(error);
    }
  }
});
